module simple-scaffolding

go 1.16

require (
	github.com/gin-gonic/gin v1.9.1
	github.com/google/uuid v1.3.0
	github.com/google/wire v0.5.0
	github.com/pkg/errors v0.9.1
	github.com/sony/sonyflake v1.1.0
	github.com/spf13/viper v1.16.0
	github.com/swaggo/files v1.0.1
	github.com/swaggo/gin-swagger v1.6.0
	github.com/swaggo/swag v1.16.2
	github.com/twilio/twilio-go v1.15.2 // indirect
	go.uber.org/zap v1.24.0
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
	gorm.io/gorm v1.25.1
	moul.io/zapgorm2 v1.3.0 // indirect
)
