package service

import (
	"simple-scaffolding/internal/model"
	"simple-scaffolding/internal/repository"
	
)

type SmsService interface {
	GetSms(id int64) (*model.Sms, error)
	SendSms(sms model.Sms) error
}

func NewSmsService(service *Service, smsRepository repository.SmsRepository) SmsService {
	return &smsService{
		Service:        service,
		smsRepository: smsRepository,
	}
}

type smsService struct {
	*Service
	smsRepository repository.SmsRepository
}

func (s *smsService) GetSms(id int64) (*model.Sms, error) {
	return s.smsRepository.FirstById(id)
}

func (s *smsService) SendSms(sms model.Sms)  error {

	

	return s.smsRepository.SendSms(sms)
}
