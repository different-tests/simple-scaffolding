package server

import (
	"simple-scaffolding/internal/handler"
	"simple-scaffolding/internal/middleware"
	"simple-scaffolding/pkg/helper/resp"
	"simple-scaffolding/pkg/log"

	"github.com/gin-gonic/gin"

	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"simple-scaffolding/docs"
)

func NewServerHTTP(
	logger *log.Logger,
	userHandler handler.UserHandler,
	smsHandler handler.SmsHandler,
) *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()

	docs.SwaggerInfo.BasePath = "/v1"
	r.GET("/swagger/*any", ginSwagger.WrapHandler(
		swaggerfiles.Handler,
		//ginSwagger.URL(fmt.Sprintf("http://localhost:%d/swagger/doc.json", conf.GetInt("app.http.port"))),
		ginSwagger.DefaultModelsExpandDepth(-1),
	))
	r.Use(
		middleware.CORSMiddleware(),
	)

	r.GET("/", func(ctx *gin.Context) {
		resp.HandleSuccess(ctx, map[string]interface{}{
			"say": "Hi Nunu!",
		})
	})
	r.GET("/user", userHandler.GetUserById)

	r.GET("/sms", smsHandler.GetSmsById)

	r.POST("/sendsms", smsHandler.SendSms)

	return r
}
