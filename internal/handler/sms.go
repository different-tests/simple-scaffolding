package handler

import (
	"simple-scaffolding/internal/model"
	"simple-scaffolding/internal/service"
	"simple-scaffolding/pkg/helper/resp"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type SmsHandler interface {
	GetSmsById(ctx *gin.Context)
	UpdateSms(ctx *gin.Context)
	SendSms(ctx *gin.Context)
}

type smsHandler struct {
	*Handler
	smsService service.SmsService
}

func NewSmsHandler(handler *Handler, smsService service.SmsService) SmsHandler {
	return &smsHandler{
		Handler:     handler,
		smsService: smsService,
	}
}
func (h *smsHandler) SendSms(ctx *gin.Context) {

	var newSms model.Sms

	if err := ctx.BindJSON(&newSms); err != nil {
        return
    }
		// var params struct {
	// 	Id int64 `form:"id" binding:"required"`
	// }
	// if err := ctx.ShouldBind(&params); err != nil {
	// 	resp.HandleError(ctx, http.StatusBadRequest, 1, err.Error(), nil)
	// 	return
	// }

	
	// sms, err := h.smsService.GetSmsById(params.Id)
	h.logger.Info("SendSms", zap.Any("sms", newSms))
	h.smsService.SendSms(newSms)
	// if err != nil {
	// 	resp.HandleError(ctx, http.StatusInternalServerError, 1, err.Error(), nil)
	// 	return
	// }
	resp.HandleSuccess(ctx, "sms")
}
func (h *smsHandler) GetSmsById(ctx *gin.Context) {
	// var params struct {
	// 	Id int64 `form:"id" binding:"required"`
	// }
	// if err := ctx.ShouldBind(&params); err != nil {
	// 	resp.HandleError(ctx, http.StatusBadRequest, 1, err.Error(), nil)
	// 	return
	// }

	
	// sms, err := h.smsService.GetSmsById(params.Id)
	h.logger.Info("GetSmsByID", zap.Any("sms", "sms"))
	// if err != nil {
	// 	resp.HandleError(ctx, http.StatusInternalServerError, 1, err.Error(), nil)
	// 	return
	// }
	resp.HandleSuccess(ctx, "sms")
}

func (h *smsHandler) UpdateSms(ctx *gin.Context) {
	resp.HandleSuccess(ctx, nil)
}
