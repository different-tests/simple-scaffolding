package model

// import "gorm.io/gorm"

type Sms struct {
	To   string `json:"to"`
	From string `json:"from"`
	Body string `json:"body"`
}

// func (m *Sms) TableName() string {
//     return "sms"
// }
