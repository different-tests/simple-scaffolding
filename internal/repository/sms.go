package repository

import (
	"encoding/json"
	"fmt"
	"simple-scaffolding/internal/model"

	twilioApi "github.com/twilio/twilio-go/rest/api/v2010"
)

type SmsRepository interface {
	FirstById(id int64) (*model.Sms, error)
	SendSms(sms model.Sms) error
}

func NewSmsRepository(repository *Repository) SmsRepository {
	return &smsRepository{
		Repository: repository,
	}
}

type smsRepository struct {
	*Repository
}

func (r *smsRepository) FirstById(id int64) (*model.Sms, error) {
	var sms model.Sms
	// TODO: query db
	return &sms, nil
}

func (r *smsRepository) SendSms(sms model.Sms) error {

	params := &twilioApi.CreateMessageParams{}
	params.SetTo(sms.To)
	params.SetFrom(sms.From)
	params.SetStatusCallback("https://sms.vedomosti.cloud/smshook")
	params.SetBody(sms.Body)

	client := r.smsclient
	resp, err := client.Api.CreateMessage(params)
	if err != nil {
		fmt.Println("Error sending SMS message: " + err.Error())
	} else {
		response, _ := json.Marshal(*resp)
		fmt.Println("Response: " + string(response))
	}
	return nil
}
