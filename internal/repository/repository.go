package repository

import (
	"simple-scaffolding/pkg/log"

	"github.com/spf13/viper"
	"github.com/twilio/twilio-go"
	"gorm.io/gorm"
)

type Repository struct {
	db *gorm.DB
	smsclient *twilio.RestClient
	//rdb    *redis.Client
	logger *log.Logger
}

func NewRepository(logger *log.Logger, db *gorm.DB, smsclient *twilio.RestClient) *Repository {
	return &Repository{
		db: db,
		//rdb:    rdb,
		smsclient: smsclient,
		logger: logger,
	}
}
func NewDb(conf *viper.Viper, l *log.Logger) *gorm.DB {

	// logger := zapgorm2.New(l.Logger)
	// logger.SetAsDefault()

	// l.Info(conf.GetString("data.sms.accountSid"))
	// TODO: init db
	//db, err := gorm.Open(mysql.Open(conf.GetString("data.mysql.user")), &gorm.Config{})
	//if err != nil {
	//	panic(err)
	//}
	//return db
	return &gorm.DB{}
}

func NewSmsClient(conf *viper.Viper, l *log.Logger) *twilio.RestClient {
	l.Info(conf.GetString("data.sms.accountSid"))
	l.Info(conf.GetString("data.sms.authToken"))
	client := twilio.NewRestClientWithParams(twilio.ClientParams{
		Username: conf.GetString("data.sms.accountSid"),
		Password: conf.GetString("data.sms.authToken"),
	})

	// return &twilio.RestClient{}
	return client
}
