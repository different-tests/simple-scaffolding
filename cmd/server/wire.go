//go:build wireinject
// +build wireinject

package main

import (
	"github.com/gin-gonic/gin"
	"simple-scaffolding/internal/handler"
	"simple-scaffolding/internal/repository"
	"simple-scaffolding/internal/server"
	"simple-scaffolding/internal/service"
	"simple-scaffolding/pkg/log"
	"github.com/google/wire"
	"github.com/spf13/viper"
)

var ServerSet = wire.NewSet(server.NewServerHTTP)

var RepositorySet = wire.NewSet(
	repository.NewDb,
	repository.NewSmsClient,
	repository.NewRepository,
	repository.NewUserRepository,
	repository.NewSmsRepository,
	
)

var ServiceSet = wire.NewSet(
	service.NewService,
	service.NewUserService,
	service.NewSmsService,
)	

var HandlerSet = wire.NewSet(
	handler.NewHandler,
	handler.NewUserHandler,
	handler.NewSmsHandler,
)	

func newApp(*viper.Viper, *log.Logger) (*gin.Engine, func(), error) {
	panic(wire.Build(
		ServerSet,
		RepositorySet,
		ServiceSet,
		HandlerSet,
	))
}
