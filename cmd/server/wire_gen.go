// Code generated by Wire. DO NOT EDIT.

//go:generate go run github.com/google/wire/cmd/wire
//go:build !wireinject
// +build !wireinject

package main

import (
	"github.com/gin-gonic/gin"
	"github.com/google/wire"
	"github.com/spf13/viper"
	"simple-scaffolding/internal/handler"
	"simple-scaffolding/internal/repository"
	"simple-scaffolding/internal/server"
	"simple-scaffolding/internal/service"
	"simple-scaffolding/pkg/log"
)

// Injectors from wire.go:

func newApp(viperViper *viper.Viper, logger *log.Logger) (*gin.Engine, func(), error) {
	handlerHandler := handler.NewHandler(logger)
	serviceService := service.NewService(logger)
	db := repository.NewDb(viperViper, logger)
	restClient := repository.NewSmsClient(viperViper, logger)
	repositoryRepository := repository.NewRepository(logger, db, restClient)
	userRepository := repository.NewUserRepository(repositoryRepository)
	userService := service.NewUserService(serviceService, userRepository)
	userHandler := handler.NewUserHandler(handlerHandler, userService)
	smsRepository := repository.NewSmsRepository(repositoryRepository)
	smsService := service.NewSmsService(serviceService, smsRepository)
	smsHandler := handler.NewSmsHandler(handlerHandler, smsService)
	engine := server.NewServerHTTP(logger, userHandler, smsHandler)
	return engine, func() {
	}, nil
}

// wire.go:

var ServerSet = wire.NewSet(server.NewServerHTTP)

var RepositorySet = wire.NewSet(repository.NewDb, repository.NewSmsClient, repository.NewRepository, repository.NewUserRepository, repository.NewSmsRepository)

var ServiceSet = wire.NewSet(service.NewService, service.NewUserService, service.NewSmsService)

var HandlerSet = wire.NewSet(handler.NewHandler, handler.NewUserHandler, handler.NewSmsHandler)
